ARG DOCKER_IMAGE=docker
ARG DOCKER_TAG=19.03.8
FROM ${DOCKER_IMAGE}:${DOCKER_TAG} as docker

FROM debian:buster-slim as kind
ARG KIND_VERSION=v0.7.0
RUN set -x && \
    apt-get update && apt-get install -y --no-install-recommends ca-certificates curl && \
    curl -L https://github.com/kubernetes-sigs/kind/releases/download/${KIND_VERSION}/kind-linux-amd64 > /usr/local/bin/kind && \
    chmod a+rx /usr/local/bin/kind 

FROM debian:buster-slim as kubectl
ARG KUBECTL_VERSION=v1.17.0
RUN set -x && \
    apt-get update && apt-get install -y --no-install-recommends ca-certificates curl && \
    curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl > /usr/local/bin/kubectl && \
    chmod a+rx /usr/local/bin/kubectl 

FROM debian:buster-slim as final
COPY --from=docker /usr/local/bin/docker /usr/local/bin/docker
COPY --from=kind /usr/local/bin/kind /usr/local/bin/kind
COPY --from=kubectl /usr/local/bin/kubectl /usr/local/bin/kubectl
RUN apt update && apt install -y --no-install-recommends coreutils
